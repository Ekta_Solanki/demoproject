# frozen_string_literal: true

Rails.application.routes.draw do
  post 'user_token' => 'user_token#create'
  post '/signin' => 'users#create'
  # root 'books#index'
  resources :books do
    collection do
      get :search
    end
    resources :reviews
  end
end

# frozen_string_literal: true

# It is generated when mailer is used
class ApplicationMailer < ActionMailer::Base
  default from: 'from@example.com'
  layout 'mailer'
end

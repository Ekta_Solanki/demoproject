# frozen_string_literal: true

# User mailer from where the mail is send from user to user.
class UserMailer < ApplicationMailer
  default from: 'solankiaaliya2@gmail.com'

  def email(user, review)
    @user = user
    @review = review
    mail(to: @user.email, subject: 'Review Mail')
  end
end

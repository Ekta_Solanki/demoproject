# frozen_string_literal: true

# Review controller which controller the reviews for book
class ReviewsController < ApplicationController
  before_action :authenticate_user
  before_action :set_book

  def index
    reviews = @book.reviews.all
    authorize reviews
    review = reviews.where.not(user: current_user.first_name)
    data = review.as_json(only: %i[review user])
    render json: { status: 200, msg: I18n.t('review_book'), data: data }
  end

  def create
    review = @book.reviews.new(review_params)
    authorize review
    review.user = current_user.first_name
    if review.save
      create_response
    else
      render json: { status: 422, msg: review.errors }
    end
  end

  private

  def review_params
    params.permit(:review)
  end

  def set_book
    @book = Book.find(params[:book_id])
  end

  def create_response
    UserMailer.email(@book.user, review).deliver_now
    data = ReviewSerializer.new(review)
    render json: { status: 200, msg: I18n.t('review_created'), data: data }
  end
end

# frozen_string_literal: true

# Book controller helps in CRUD process of Book
class BooksController < ApplicationController
  include BooksHelper
  before_action :authenticate_user, :set_locale
  before_action :set_book, only: %i[update destroy show]

  def index
    index_book_author if current_user.role == 'author'
    index_book_visitor if current_user.role == 'visitor'
  end

  def create
    book = current_user.books.new(book_params)
    authorize book
    if book.save
      data = BookSerializer.new(book).attributes
      render json: { status: 200, msg: I18n.t('book_created'), data: data }
    else
      render json: { status: 422, msg: book.errors }
    end
  end

  def update
    authorize @book
    if @book.update_attributes(book_params)
      data = BookSerializer.new(@book).attributes
      render json: { status: 200, msg: I18n.t('book_updated'), data: data }
    else
      render json: { status: 404, msg: 'Book not updated', error: @book.errors }
    end
  end

  def show
    show_book_to_author if current_user.role == 'author'
    show_book_to_visitor if current_user.role == 'visitor'
  end

  def destroy
    authorize @book
    @book.destroy
    render json: { status: 204, msg: I18n.t('book_destroy') }
  end

  private

  def book_params
    params.permit(:name, :description, :category, :status)
  end

  def set_book
    @book = Book.find(params[:id])
  end

  def set_locale
    I18n.locale = params[:locale] || I18n.default_locale
  end
end

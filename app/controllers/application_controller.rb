# frozen_string_literal: true

# Code written here can be accessible from all controller
class ApplicationController < ActionController::API
  include Pundit
  include Knock::Authenticable
  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  private

  def user_not_authorized
    render json: { status: 401,
                   msg: I18n.t('unauthorized') }
  end
end

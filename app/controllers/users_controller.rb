# frozen_string_literal: true

# Once can create user from User controller
class UsersController < ApplicationController
  before_action :authenticate_user, only: %i[index update]
  before_action :authorize, only: [:update]
  def create
    user = User.new(user_params)
    user.add_role(params[:role])
    if user.save
      render json: { status: 200, msg: I18n.t('user_created') }
    else
      render json: { status: 204, msg: user.errors }
    end
  end

  private

  def user_params
    params.permit(:email,
                  :password,
                  :password_confirmation,
                  :phone_number,
                  :first_name,
                  :last_name,
                  :role)
  end
end

# frozen_string_literal: true

# It is a book helper which help the controller of book
module BooksHelper
  def index_book_author
    book = Book.all.paginate page: params[:page], per_page: 10
    authorize book
    book = Book.where(user_id: current_user) if params[:my_book] == '1'
    data = book.as_json(except: [:reviews])
    render json: { status: 200, msg: I18n.t('book_author'), data: data }
  end

  def index_book_visitor
    book = Book.all.order(created_at: :desc).where(status: 'active')
    authorize book
    book = if params[:more_book] == '1'
             book.paginate page: params[:page], per_page: 10
           else
             book.first(5)
           end
    data = book.as_json(only: %i[name created_at])
    render json: { status: 200, msg: I18n.t('book_visitor'), data: data }
  end

  def show_book_to_author
    book = current_user.books.find(params[:id])
    data = BookSerializer.new(book).attributes
    render json: { status: 200, msg: I18n.t('book_show'), data: data }
  end

  def show_book_to_visitor
    if @book.status == 'active'
      data = BookSerializer.new(@book), only(:name)
      render json: { status: 200, msg: I18n.t('book_show'), data: data }
    else
      render json: { status: 401, msg: I18n.t('not_active') }
    end
  end
end

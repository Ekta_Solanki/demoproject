# frozen_string_literal: true

# Policy for review which authorized the user
class ReviewPolicy < ApplicationPolicy
  attr_reader :user, :record
  def initialize(user, record)
    @user = user
    @record = record
  end

  def index?
    true if user.has_role? :visitor
  end

  def show?
    true if user.has_role? :visitor
  end

  def create?
    true if user.has_role? :visitor
  end

  # def new?
  #   create?
  # end

  def update?
    false
  end

  # def edit?
  #   update?
  # end

  def destroy?
    false
  end

  private

  def review
    record
  end
end

# frozen_string_literal: true

# Policy for book to authorize users.
class BookPolicy < ApplicationPolicy
  attr_reader :user, :record
  def initialize(user, record)
    @user = user
    @record = record
  end

  def index?
    true if user.present?
  end

  def show?
    true
  end

  def create?
    true if user.has_role? :author
  end

  # def new?
  #   create?
  # end

  def update?
    true if user.role == 'author' && user == book.user
  end

  # def edit?
  #   update?
  # end

  def destroy?
    true if user.role == 'author' && user == book.user
  end

  private

  def book
    record
  end
end

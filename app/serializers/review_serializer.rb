# frozen_string_literal: true

# Review Serializer serializes the review data
class ReviewSerializer < ActiveModel::Serializer
  attributes :user, :review
end

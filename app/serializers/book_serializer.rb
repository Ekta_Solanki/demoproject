# frozen_string_literal: true

# Book Serializer serializes the book data
class BookSerializer < ActiveModel::Serializer
  attributes :name,
             :published_at,
             :description,
             :status,
             :category

  has_many :reviews

  def reviews
    object.reviews.where.not(user: current_user.first_name)
  end
end

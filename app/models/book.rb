# frozen_string_literal: true

# Book model where book attributes, attributes validation
# and association is define
class Book < ApplicationRecord
  # Association
  belongs_to :user
  has_many :reviews, dependent: :destroy

  accepts_nested_attributes_for :reviews

  # Validation
  validates_presence_of :name, :description, :category, :status
  validates_length_of :name, maximum: 120
  validates_length_of :description, maximum: 250

  # Enum
  enum category: { novel: 'Novel', horror: 'Horror', cooking: 'Cooking',
                   comics: 'Comics', history: 'History' }
  enum status: { active: 'Active', inactive: 'Inactive' }

  validates :category, inclusion: { in: :category, message: 'Invalid Category' }
  validates :status, inclusion: { in: :status, message: 'Invalid Status' }

  # Alias
  alias_attribute :published_at, :created_at
end

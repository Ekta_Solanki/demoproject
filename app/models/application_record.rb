# frozen_string_literal: true

# Application record is by default generated
class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true
end

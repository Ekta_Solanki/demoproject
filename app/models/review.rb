# frozen_string_literal: true

# Review model where the attribute and association is define
class Review < ApplicationRecord
  belongs_to :book
  validates_presence_of :review
end

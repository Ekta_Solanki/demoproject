# frozen_string_literal: true

# User model where the attributes association and validation for user is define
class User < ApplicationRecord
  rolify
  # Association
  has_many :books, dependent: :destroy

  # Validation
  has_secure_password
  validates_uniqueness_of :email
  validates_presence_of :email, :password_digest
  validates_confirmation_of :password

  # Method to make email id lower case
  before_validation do
    self .email = email.to_s.downcase
  end
end

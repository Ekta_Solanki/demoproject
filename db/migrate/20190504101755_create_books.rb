# frozen_string_literal: true

# To create a Book and association with user
class CreateBooks < ActiveRecord::Migration[5.2]
  def change
    create_table :books do |t|
      t.string :name
      t.string :description
      t.string :category
      t.string :status
      t.references :user

      t.timestamps
    end
  end
end

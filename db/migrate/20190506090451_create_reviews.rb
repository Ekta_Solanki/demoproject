# frozen_string_literal: true

# Review table associated with book
class CreateReviews < ActiveRecord::Migration[5.2]
  def change
    create_table :reviews do |t|
      t.string :review
      t.references :book

      t.timestamps
    end
  end
end

# frozen_string_literal: true

# Added the column user to keep the user name which user has review hw book
class AddColumnToReview < ActiveRecord::Migration[5.2]
  def change
    add_column :reviews, :user, :string
  end
end
